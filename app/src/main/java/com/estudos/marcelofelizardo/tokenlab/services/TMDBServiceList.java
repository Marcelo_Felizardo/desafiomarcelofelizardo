package com.estudos.marcelofelizardo.tokenlab.services;

import com.estudos.marcelofelizardo.tokenlab.domain.model.Movie;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface TMDBServiceList {
    public static final String URL_Base = "https://desafio-mobile.nyc3.digitaloceanspaces.com/";

    @GET("movies")
    Call<List<Movie>> listCalalog();
}
