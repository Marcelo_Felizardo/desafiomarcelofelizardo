package com.estudos.marcelofelizardo.tokenlab.ui.fragments;

import android.content.Context;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.estudos.marcelofelizardo.tokenlab.R;
import com.estudos.marcelofelizardo.tokenlab.domain.controllers.AdapterFrag1;
import com.estudos.marcelofelizardo.tokenlab.domain.model.Movie;
import com.estudos.marcelofelizardo.tokenlab.services.TMDBServiceList;
import com.estudos.marcelofelizardo.tokenlab.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.List;

import static com.estudos.marcelofelizardo.tokenlab.services.TMDBServiceList.URL_Base;

public class Fragment1 extends Fragment {
    private Button button;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private AppCompatActivity activity;
    private Context context;

    public Fragment1() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_fragment1, container, false);

        progressBar = view.findViewById(R.id.progress_bar1);
        progressBar.setVisibility(view.VISIBLE);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_frag1);

        if (!NetworkUtils.isOnline(view.getContext())) {
            Toast.makeText(view.getContext(), "Internet error. Check your connection.", Toast.LENGTH_LONG).show();
        }else{
            final Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(URL_Base)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            TMDBServiceList tmdbService = retrofit.create(TMDBServiceList.class);
            Call<List<Movie>> requestCatalog = tmdbService.listCalalog();

            requestCatalog.enqueue(new Callback<List<Movie>>() {
                @Override
                public void onResponse(Call<List<Movie>> call, Response<List<Movie>> response) {
                    if(!response.isSuccessful()){
                        Toast.makeText(getActivity(), "Error code " + response.code(), Toast.LENGTH_LONG).show();
                    }else{
                        progressBar.setVisibility(view.GONE);
                        List<Movie> movieList = new ArrayList<>();
                        movieList = response.body();

                        recyclerView.setVisibility(view.VISIBLE);
                        recyclerView.setAdapter(new AdapterFrag1(movieList, (AppCompatActivity) getActivity()));
                        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                    }
                }

                @Override
                public void onFailure(Call<List<Movie>> call, Throwable t) {
                    Toast.makeText(getActivity(), "Error " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }

        return view;
    }
}
