package com.estudos.marcelofelizardo.tokenlab.domain.controllers;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.estudos.marcelofelizardo.tokenlab.R;

public class MyViewHolder extends RecyclerView.ViewHolder{
    public ImageView img_url;
    public TextView txt_title;
    public TextView txt_genres;
    public TextView txt_vote;

    public MyViewHolder(@NonNull View itemView) {
        super(itemView);

        this.img_url = (ImageView)itemView.findViewById(R.id.image_poster_url);
        this.txt_title = (TextView)itemView.findViewById(R.id.textTitle);
        this.txt_genres = (TextView)itemView.findViewById(R.id.textGenres);
        this.txt_vote = (TextView)itemView.findViewById(R.id.textVote);
    }
}
