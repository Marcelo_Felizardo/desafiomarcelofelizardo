package com.estudos.marcelofelizardo.tokenlab.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.estudos.marcelofelizardo.tokenlab.R;
import com.estudos.marcelofelizardo.tokenlab.domain.controllers.ChangeFragment;
import com.estudos.marcelofelizardo.tokenlab.ui.fragments.Fragment1;
import com.estudos.marcelofelizardo.tokenlab.utils.NetworkUtils;

public class MainActivity extends AppCompatActivity {
    private Button buttonFrag1;
    private ImageView imageView1, imageView2, imageView3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!NetworkUtils.isOnline(this)) {
            Toast.makeText(this, "Internet error. Check your connection.", Toast.LENGTH_LONG).show();
        }

        imageView1 = (ImageView) findViewById(R.id.imageView1);
        imageView2 = (ImageView) findViewById(R.id.imageView2);
        imageView3 = (ImageView) findViewById(R.id.imageView3);

        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showListMovies();
            }
        });
        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showListMovies();
            }
        });
        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showListMovies();
            }
        });
    }

    private void showListMovies() {
        Fragment1 fragment1 = new Fragment1();
        ChangeFragment.toChange(this, fragment1, R.id.container_activity);
    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount() > 0){
            super.onBackPressed();
        }else{
            finish();
        }
    }
}
