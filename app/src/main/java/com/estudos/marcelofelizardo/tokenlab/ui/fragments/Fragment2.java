package com.estudos.marcelofelizardo.tokenlab.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.estudos.marcelofelizardo.tokenlab.R;
import com.estudos.marcelofelizardo.tokenlab.domain.model.Movie;
import com.estudos.marcelofelizardo.tokenlab.services.TMDBServiceMovie;
import com.estudos.marcelofelizardo.tokenlab.utils.NetworkUtils;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.estudos.marcelofelizardo.tokenlab.services.TMDBServiceMovie.URL_BaseMovie;

public class Fragment2 extends Fragment {
    private View view;
    private TextView txtTitle;
    private TextView txtOverview;
    private TextView txtOverviewFixed;
    private ProgressBar progressBar;

    public Fragment2() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_fragment2, container, false);

        progressBar = view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(view.VISIBLE);

        final String movieId = getArguments().getString("movieId");
        final ImageView imageViewFrag2 = view.findViewById(R.id.imageViewFrag2);
        txtTitle = view.findViewById(R.id.textViewTitleFrag2);
        txtOverview = view.findViewById(R.id.textViewOverviewFrag2);
        txtOverviewFixed = view.findViewById(R.id.textViewOverviewFixed);

        if (!NetworkUtils.isOnline(view.getContext())) {
            Toast.makeText(view.getContext(), "Internet error. Check your connection.", Toast.LENGTH_LONG).show();
        }else{
            final Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(URL_BaseMovie)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            TMDBServiceMovie tmdbServiceMovie = retrofit.create(TMDBServiceMovie.class);
            Call<Movie> requestCatalog = tmdbServiceMovie.dataMovie(movieId);

            requestCatalog.enqueue(new Callback<Movie>() {
                @Override
                public void onResponse(Call<Movie> call, Response<Movie> response) {
                    if(!response.isSuccessful()){
                        Toast.makeText(getActivity(), "Error code " + response.code(), Toast.LENGTH_LONG).show();
                    }else{
                        progressBar.setVisibility(view.GONE);
                        txtTitle.setVisibility(view.VISIBLE);
                        txtOverview.setVisibility(view.VISIBLE);
                        txtOverviewFixed.setVisibility(view.VISIBLE);

                        Movie movie = new Movie();
                        movie = response.body();

                        Picasso.get().load(movie.getPoster_url()).into(imageViewFrag2);
                        txtTitle.setText(movie.getTitle());
                        txtOverview.setText(movie.getOverview());
                    }
                }

                @Override
                public void onFailure(Call<Movie> call, Throwable t) {

                }
            });
        }

        return view;
    }
}
