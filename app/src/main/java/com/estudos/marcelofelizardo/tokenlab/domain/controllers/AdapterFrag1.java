package com.estudos.marcelofelizardo.tokenlab.domain.controllers;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.estudos.marcelofelizardo.tokenlab.R;
import com.estudos.marcelofelizardo.tokenlab.domain.model.Movie;
import com.estudos.marcelofelizardo.tokenlab.ui.fragments.Fragment2;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterFrag1 extends RecyclerView.Adapter<MyViewHolder>{
    private AppCompatActivity activity;
    private List<Movie> listMovies;

    public AdapterFrag1 (List<Movie> listMovies, AppCompatActivity activity){
        this.listMovies = listMovies;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(activity);
        View view = layoutInflater.inflate(R.layout.item_film, viewGroup, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        final Movie movie = listMovies.get(i);

        String string = " ";
        for (int j=0; j < movie.getGenres().length; j++){
            string += movie.getGenres()[j] + "  ";
        }

        Picasso.get().load(movie.getPoster_url()).into(myViewHolder.img_url);
        myViewHolder.txt_title.setText(movie.getTitle());
        myViewHolder.txt_genres.setText(" Genero:\n" + string);
        myViewHolder.txt_vote.setText(" Nota: " + movie.getVote_average());

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLMovieSelected(movie);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listMovies.size();
    }

    private void showLMovieSelected(Movie movie) {
        Bundle bundle = new Bundle();
        bundle.putString("movieId", String.valueOf(movie.getId()));
        Fragment2 fragment2 = new Fragment2();
        fragment2.setArguments(bundle);
        ChangeFragment.toChange((AppCompatActivity)activity, fragment2, R.id.container_activity);
    }
}
