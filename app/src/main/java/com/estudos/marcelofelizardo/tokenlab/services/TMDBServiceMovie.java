package com.estudos.marcelofelizardo.tokenlab.services;

import com.estudos.marcelofelizardo.tokenlab.domain.model.Movie;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface TMDBServiceMovie {
    public static final String URL_BaseMovie = "https://desafio-mobile.nyc3.digitaloceanspaces.com/";

    @GET("movies/{movieId}")
    Call<Movie> dataMovie(@Path("movieId") String movieId);
}
